angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Did you get the ice cream?',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
}).factory('Phones', function ($http, $q, $ionicPopup, $state ) {
        return {
            all: function () {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: 'http://mairon-studio.ru/wp-content/plugins/crm_mairon/api_crm_mairon.php',
                    data: {action: 'getPhones'},
                    responseType: 'json'
                }).success(function (array) {
                    deferred.resolve(array);
                    console.log(array);
                });
                deferred.promise.then(function (data) {
                        return data;
                    }
                );
                return deferred.promise;
            },
            random: function () {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: 'http://mairon-studio.ru/wp-content/plugins/crm_mairon/api_crm_mairon.php',
                    data: {action: 'getPhonesRandom'}
                    //responseType: 'json'
                }).success(function (array) {
                    deferred.resolve(array);
                });
                deferred.promise.then(function (data) {
                        return data;
                    }
                );
                return deferred.promise;
            },
            setStatus: function (phone) {
                phone.action = 'PhoneSetStatus';

                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: 'http://mairon-studio.ru/wp-content/plugins/crm_mairon/api_crm_mairon.php',
                    data: phone
                }).success(function (array) {
                    deferred.resolve(array);
                });
                deferred.promise.then(function (data) {
                        return data;
                    }
                );
                return deferred.promise;
            },
            remove: function (phones, phone) {
                phones.splice(phones.indexOf(phone), 1);
            },
            get: function (phones, id_phone) {
                for (var i = 0; i < phones.length; i++) {
                    if (phones[i].id_phone === parseInt(id_phone)) {
                        return phones[i];
                    }
                }
                return null;
            }
        };
    });