angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('DashCtrl', function ($scope, Phones, phone, $state) {
        $scope.phone = phone;
        console.log(phone);
        $scope.changeStatus = function (phone, newStatus) {
            phone.status_phone = newStatus;
            Phones.setStatus(phone);
            Phones.random().then(function (phone) {
                $scope.phone = phone;
            });

        }
    })

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
